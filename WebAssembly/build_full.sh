#!/bin/sh
# Note that the JS file that this produces is not the one you should use,
# there's just no good way to build this without producing it.
emcc full.c -Os -s WASM=1 -o full.js && base64 -w0 full.wasm > full.wasm.base64 && rm full.js
