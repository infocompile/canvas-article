#!/bin/sh
# Note that the JS file that this produces is not the one you should use,
# there's just no good way to build this without producing it.
emcc minimal.c -Os -s WASM=1 -o minimal.js && base64 -w0 minimal.wasm > minimal.wasm.base64 && rm minimal.js
