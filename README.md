# Canvas article

Code files for the canvas article.
Players directory contains the version run in the article, language folders contain their respective implementations
that are used as snippets in the article. WebAssembly JS files contain the compiled WA in base64 encoded form,
which can be produced by running the build scripts in the WebAssembly directory. The JS files created by the
build scripts are not the ones you should use, instead use the ones that end in 'with_base64'.
