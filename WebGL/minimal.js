// Get 2d drawing context
const canvas = document.getElementById('c');
const gl = canvas.getContext('webgl');
gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
const buffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
const vertexCount = 6;
const vertexLocations = [
  // X, Y
  -1.0, -1.0,
   1.0, -1.0,
  -1.0,  1.0,
  -1.0,  1.0,
   1.0, -1.0,
   1.0,  1.0
];

gl.bufferData(
  gl.ARRAY_BUFFER,
  new Float32Array(vertexLocations),
  gl.STATIC_DRAW
);

const program = gl.createProgram();
const buildShader = (type, source) => {
  const shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  gl.attachShader(program, shader);
};
buildShader(
  gl.VERTEX_SHADER,
  `
attribute vec2 a_position;
void main() {
  gl_Position = vec4(a_position, 0.0, 1.0);
}`
);

buildShader(
  gl.FRAGMENT_SHADER,
  `
void main() {
  gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}`
);
gl.linkProgram(program);
gl.useProgram(program);
const positionLocation = gl.getAttribLocation(program, 'a_position');
gl.enableVertexAttribArray(positionLocation);
const fieldCount = vertexLocations.length / vertexCount;
gl.vertexAttribPointer(
  positionLocation,
  fieldCount,
  gl.FLOAT,
  gl.FALSE,
  fieldCount * Float32Array.BYTES_PER_ELEMENT,
  0
);
// Draw
gl.drawArrays(gl.TRIANGLES, 0, vertexCount);

