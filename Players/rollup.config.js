import rollup from "rollup";
import babel from "rollup-plugin-babel";
import { uglify } from "rollup-plugin-uglify";

export default {
  output: {
    file: "dist/players.js",
    format: "iife"
  },
  input: "index.js",
  plugins: [
    babel({
      presets: ['@babel/preset-env']
    }),
    uglify({})
  ]
};
