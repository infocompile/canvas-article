// This assumes that there are three elements in the dom with id:s jsPlayer, waPlayer and gaPlayer.
const ACTIVITY_CLASS = 'user-is-active';

const isVisible = (element) => {
  if (Document.hidden) {
    return false;
  }
  const rect = element.getBoundingClientRect();
  return rect.top < window.innerHeight && rect.bottom >= 0;
};

// TODO check for wa/gl support, show error if not supported
const CanvasPlayer = (title, wrapper, rendererFn, width, height) => {
  let playing = false;
  let onPlayHandler;
  let renderer;
  // Mark user activity with a class, this allow us to show the play button when user is active.
  let activityTimer;
  let pauseTimestamp;
  let playOffset = 0;
  wrapper.classList.add('canvasWrapper');
  wrapper.onclick = () => {
    window.clearTimeout(activityTimer);
    wrapper.classList.add(ACTIVITY_CLASS);
    activityTimer = window.setTimeout(() => {
      wrapper.classList.remove(ACTIVITY_CLASS);
    }, 1000);
  };
  wrapper.onmousemove = wrapper.onclick;
  // Create player UI elements
  const titleSpan = document.createElement('span');
  titleSpan.innerHTML = title;
  wrapper.appendChild(titleSpan);
  const fpsOutput = document.createElement('output');
  fpsOutput.setAttribute('for', `${title}_fps`);
  wrapper.appendChild(fpsOutput);
  const playPauseButton = document.createElement('button');
  playPauseButton.setAttribute('type', 'button');
  playPauseButton.appendChild(document.createElement('i'));
  wrapper.appendChild(playPauseButton);

  const pause = () => {
    playing = false;
    playPauseButton.title = 'Play';
    wrapper.classList.remove('playing');
  };

  let frameCounter = 0;
  let previousTimestamp;

  const fps = (timestamp) => {
    frameCounter += 1;
    if (frameCounter === 11) {
      const delta = timestamp - previousTimestamp;
      fpsOutput.value = `${(10000 / delta).toFixed(1)} fps`;
      frameCounter = 1;
      // Let's misuse this to pause the animation if canvas is offscreen
      if (!isVisible(wrapper)) {
        pause();
      }
    }
    if (frameCounter === 1) {
      previousTimestamp = timestamp;
    }
  };

  const render = (timestamp) => {
    if (!playing) {
      pauseTimestamp = timestamp - playOffset;
      return;
    }
    fps(timestamp);
    if (pauseTimestamp) {
      // Offset our timestamp so that the animation continues where we left off
      playOffset = timestamp - pauseTimestamp;
      pauseTimestamp = null;
    }
    renderer.render(timestamp - playOffset);
    requestAnimationFrame(render);
  };
  
  const setOnPlayHandler = (handler) => {
    onPlayHandler = handler;
  };

  let play;

  let ret = {
    play,
    pause,
    setOnPlayHandler
  };

  play = () => {
    if (onPlayHandler) {
      onPlayHandler(ret);
    }
    playing = true;
    playPauseButton.title = 'Pause';
    wrapper.classList.add('playing');
    requestAnimationFrame((timestamp) => {
      render(timestamp);
    });
  };

  playPauseButton.onclick = () => {
    if (playing) {
      pause();
    } else {
      play();
    }
  };

  // Set up canvas and init renderer
  const canvas = document.createElement('canvas');
  canvas.setAttribute('id', `${title}_fps`);
  canvas.height = height;
  canvas.width = width;
  wrapper.appendChild(canvas);
  try {
    renderer = rendererFn(canvas);
  } catch (errorText) {
    playPauseButton.parentNode.removeChild(playPauseButton);
    const errorEl = document.createElement('div');
    errorEl.className = 'error';
    errorEl.innerHTML = errorText;
    wrapper.appendChild(errorEl);
  }

  return ret;
};

/*
Note that this will break in big endian systems, see
https://hacks.mozilla.org/2017/01/typedarray-or-dataview-understanding-byte-order
*/
const jsRenderer = (canvas) => {
  const RAD = 2 * Math.PI;
  const BLADES = 3;
  const CYCLE_WIDTH = 100;
  const BLADES_T_CYCLE_WIDTH = BLADES * CYCLE_WIDTH;
  const height = canvas.height;
  const width = canvas.width;
  const ch = height / 2;
  const cw = width / 2;
  const maxDistance = Math.sqrt((ch * ch) + (cw * cw));
  // Disabling alpha seems to give a slight boost. Image data still includes alpha though.
  const ctx = canvas.getContext(
    '2d',
    {
      alpha: false,
      antialias: false,
      depth: false
    }
  );

  if (!ctx) {
    throw 'Your browser does not support canvas';
  }
  if (!ArrayBuffer) {
    throw 'Your browser does not support ArrayBuffer';
  }
  if (!Uint8ClampedArray) {
    throw 'Your browser does not support Uint8ClampedArray';
  }
  if (!Uint32Array) {
    throw 'Your browser does not support Uint32Array';
  }

  const imageData = ctx.getImageData(0, 0, width, height);

  // Create a buffer that's the same size as our image
  const buf = new ArrayBuffer(imageData.data.length);
  // 'Live' 8 bit clamped view to our array, we'll use this for writing to the canvas
  const buf8 = new Uint8ClampedArray(buf);
  // 'Live' 32 bit view into our array, we'll use this for drawing
  const buf32 = new Uint32Array(buf);

  const render = (timestamp) => {
    // Flooring this makes things a whole lot faster in both FF and Chrome
    // 2000 added to timestamp out of pure laziness... without it we get some weird visuals in the beginning
    const scaledTimestamp = Math.floor((timestamp / 10.0) + 2000.0);
    for (let y = 0; y < height; y += 1) {
      const dy = ch - y;
      const dysq = dy * dy;
      const yw = y * width;
      for (let x = 0; x < width; x += 1) {
        const dx = cw - x;
        const dxsq = dx * dx;
        const angle = Math.atan2(dx, dy) / RAD;
        // Arbitrary mangle of the distance, just something that looks pleasant
        const asbs = dxsq + dysq;
        const distanceFromCenter = Math.sqrt(asbs);
        const scaledDistance = (asbs / 400.0) + distanceFromCenter;
        const lerp =
          1.0 - (
            (Math.abs(
              (scaledDistance - scaledTimestamp) + (angle * BLADES_T_CYCLE_WIDTH)
            ) %
            CYCLE_WIDTH) /
            CYCLE_WIDTH);
        // Fade R more slowly
        const absoluteDistanceRatioGB = 1.0 - (distanceFromCenter / maxDistance);
        const absoluteDistanceRatioR = (absoluteDistanceRatioGB * 0.8) + 0.2;
        // Don't round these, it makes things slower
        const fadeB = 50.0 * lerp * absoluteDistanceRatioGB;
        const fadeR =
          240.0 * lerp * absoluteDistanceRatioR * (1.0 + lerp) * 0.5;
        const fadeG = 120.0 * lerp * lerp * lerp * absoluteDistanceRatioGB;
        buf32[yw + x] =
          (255 << 24) |   // A
          (fadeB << 16) | // B
          (fadeG << 8) |  // G
          fadeR;          // R
      }
    }
    // Write our data back to the canvas
    imageData.data.set(buf8);
    ctx.putImageData(imageData, 0, 0);
  };
  return {
    render
  };
};

const waRenderer = (canvas) => {

  if (!WebAssembly) {
    throw 'Your browser does not support WebAssembly';
  }
  if (!ArrayBuffer) {
    throw 'Your browser does not support ArrayBuffer';
  }
  if (!Uint8Array) {
    throw 'Your browser does not support Uint8Array';
  }
  if (!Uint8ClampedArray) {
    throw 'Your browser does not support Uint8ClampedArray';
  }
  // Contains the actual webassembly
  const base64data = 'AGFzbQEAAAABFgRgAn9/AX9gAn9/AXxgAXwAYAF8AXwCEgEDZW52Bm1lbW9yeQIBgAKAAgMFBAMCAQAHEwIFX2luaXQAAwdfcmVuZGVyAAEKowUEKQAgAEQAAAAAAADgP6CcIABEAAAAAAAA4D+hmyAARAAAAAAAAAAAZhsLogMCDH8DfEGMrOgDKAIAIgZBAEoEQEGQrOgDKAIAIQdBiKzoAygCACIEQQBKIQhBlKzoAygCACEJIABEAAAAAAAAJECjRAAAAAAAQJ9AoJyqtyEOQYCs6AMrAwAhDwNAIAcgA2siBSAFbCEKIAQgA2whCyAIBEBBACEBA0AgCSABayICIAJsIApqtyIAnyENRAAAAAAAAPA/IAIgBRACRBgtRFT7IRlAo0QAAAAAAMByQKIgAEQAAAAAAAB5QKMgDaAgDqGgmSIAIABEAAAAAAAAWUCjnEQAAAAAAABZQKKhRAAAAAAAAFlAo6EiAEQAAAAAAABJQKJEAAAAAAAA8D8gDSAPo6EiDaIQAKohAiAARAAAAAAAAPA/oCAARAAAAAAAAG5AoiANRJqZmZmZmek/okSamZmZmZnJP6CiokQAAAAAAADgP6IQAKohDCABIAtqQQJ0QYAIaiAAIAAgAEQAAAAAAABeQKKioiANohAAqkEIdCACQRB0ciAMckGAgIB4cjYCACABQQFqIgEgBEcNAAsLIANBAWoiAyAGSA0ACwsLhQEBA3wgAEEAIABrIABBf0obt0S7vdfZ33zbPaAhAiABtyEDIAFBf0oEfEQYLURU+yHpPyEEIAMgAqEgAiADoKMFRNIhM3982QJAIQQgAiADoCACIAOhowsiAiACIAJE4zYawFsgyT+ioqIgAkRgdk8eFmrvP6KhIASgIgKaIAIgAEEASBsLTABBiKzoAyAANgIAQYys6AMgATYCAEGQrOgDIAFBAXUiATYCAEGUrOgDIABBAXUiADYCAEGArOgDIAEgAWwgACAAbGq3nzkDAEGACAs=';
  const decode = (b64) => {
    const str = window.atob(b64);
    const array = new Uint8Array(str.length);
    for (let i = 0; i < str.length; i += 1) {
      array[i] = str.charCodeAt(i);
    }
    return array.buffer;
  };
  const memSize = 256;
  const memory = new WebAssembly.Memory({ initial: memSize, maximum: memSize });

  const instance = new WebAssembly.Instance(
    new WebAssembly.Module(new Uint8Array(decode(base64data))),
    { env: { memory } }
  );
  const height = canvas.height;
  const width = canvas.width;
  // Disabling alpha seems to give a slight boost. Image data still includes alpha though.
  const ctx = canvas.getContext(
    '2d',
    {
      alpha: false,
      antialias: false,
      depth: false
    }
  );

  if (!ctx) {
    throw 'Your browser does not support canvas';
  }

  const pointer = instance.exports._init(width, height);
  const data = new Uint8ClampedArray(memory.buffer, pointer, width * height * 4);
  const img = new ImageData(data, width, height);

  const render = (timestamp) => {
    instance.exports._render(timestamp);
    ctx.putImageData(img, 0, 0);
  };

  return {
    render
  };
};

const glRenderer = (canvas) => {
  if (!Float32Array) {
    throw 'Your browser does not support Float32Array';
  }

  const height = canvas.height;
  const width = canvas.width;
  // Disabling alpha seems to give a slight boost. Image data still includes alpha though.
  const webglOptions = {
    // alpha: false, this would break the rounded corners in chrome
    antialias: false,
    depth: false
  };

  let gl = canvas.getContext(
    'webgl',
    webglOptions
  );

  if (!gl) {
    // webgl not available, show error
    gl = canvas.getContext(
      'experimental-webgl',
      webglOptions
    );
  }

  if (!gl) {
    throw 'Your browser does not support WebGL';
  }

  gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
  // Create a buffer, make it current
  const buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  const vertexCount = 6;
  // Coordinates are -1 - 1, Y is inverted.
  // Two triangles forming a rectangle that covers the viewport
  const vertexLocations = [
    // X, Y
    -1.0, -1.0,
     1.0, -1.0,
    -1.0,  1.0,
    -1.0,  1.0,
     1.0, -1.0,
     1.0,  1.0
  ];
  // Put vertex locations into buffer, hint that we won't be touching it
  gl.bufferData(
    gl.ARRAY_BUFFER,
    new Float32Array(vertexLocations),
    gl.STATIC_DRAW
  );

  const program = gl.createProgram();
  const buildShader = (type, source) => {
    const shader = gl.createShader(type);
    gl.shaderSource(shader, source);
    gl.compileShader(shader);
    gl.attachShader(program, shader);
    return shader;
  };

  const vertexShader = buildShader(
    gl.VERTEX_SHADER,
    `
attribute vec2 a_position;
void main() {
  gl_Position = vec4(a_position, 0.0, 1.0);
}`
  );

  const fragmentShader = buildShader(
    gl.FRAGMENT_SHADER,
    `
  #define M_RAD ${Math.PI * 2}
  #define CYCLE_WIDTH 100.0
  #define BLADES 3.0
  #define BLADES_T_CYCLE_WIDTH 300.0
  // Mobiles need this
  precision highp float;
  uniform float timestamp;
  void main() {
    vec2 center = vec2(${width / 2}, ${height / 2});
    float maxDistance = length(vec2(center.x, center.y));
    float scaledTimestamp = floor(timestamp / 10.0 + 2000.0);
    vec2 d = center.xy - gl_FragCoord.xy;
    vec2 dsq = pow(d, vec2(2.0));
    // Flipped axis to counteract flipped Y
    float angle = atan(d.y, d.x) / M_RAD;
    // Arbitrary mangle of the distance, just something that looks pleasant
    float asbs = dsq.x + dsq.y;
    float distanceFromCenter = sqrt(asbs);
    float scaledDistance = (asbs / 400.0) + distanceFromCenter;
    float lerp = 1.0 - mod(abs(scaledDistance - scaledTimestamp + angle * BLADES_T_CYCLE_WIDTH), CYCLE_WIDTH) / CYCLE_WIDTH;
    // Fade R more slowly
    float absoluteDistanceRatioGB = (1.0 - distanceFromCenter / maxDistance);
    float absoluteDistanceRatioR = absoluteDistanceRatioGB * 0.8 + 0.2;

    float fadeB = (50.0 / 255.0) * lerp * absoluteDistanceRatioGB;
    float fadeR = (240.0 / 255.0) * lerp * absoluteDistanceRatioR * (1.0 + lerp) / 2.0;
    float fadeG = (120.0 / 255.0) * lerp * lerp * lerp * absoluteDistanceRatioGB;
    gl_FragColor = vec4(fadeR, fadeG, fadeB, 1.0);
  }`
  );
  gl.linkProgram(program);
  gl.useProgram(program);
  // Detach and delete shaders as they're no longer needed
  gl.detachShader(program, vertexShader);
  gl.detachShader(program, fragmentShader);
  gl.deleteShader(vertexShader);
  gl.deleteShader(fragmentShader);
  // Add attribute pointer to our vertex locations
  const positionLocation = gl.getAttribLocation(program, 'a_position');
  gl.enableVertexAttribArray(positionLocation);
  const fieldCount = vertexLocations.length / vertexCount;
  gl.vertexAttribPointer(
    positionLocation,
    fieldCount,
    gl.FLOAT,
    gl.FALSE,
    fieldCount * Float32Array.BYTES_PER_ELEMENT,
    0
  );

  const timestampId = gl.getUniformLocation(program, 'timestamp');

  const render = (timestamp) => {
    // Update timestamp
    gl.uniform1f(timestampId, timestamp);
    // Draw
    gl.drawArrays(gl.TRIANGLES, 0, vertexCount);
  };

  return {
    render
  };
};

const playerInstances = [];

const onPlayHandler = (playedInstance) => {
  playerInstances.forEach((playerInstance) => {
    if (playerInstance !== playedInstance) {
      playerInstance.pause();
    }
  });
};

const w = 640;
const h = 360;

const jsPlayer = CanvasPlayer(
  'Javascript',
  document.getElementById('jsPlayer'),
  jsRenderer,
  w,
  h
);
playerInstances.push(jsPlayer);

const waPlayer = CanvasPlayer(
  'WebAssembly',
  document.getElementById('waPlayer'),
  waRenderer,
  w,
  h
);
playerInstances.push(waPlayer);

const glPlayer = CanvasPlayer(
  'WebGL',
  document.getElementById('glPlayer'),
  glRenderer,
  w,
  h
);
playerInstances.push(glPlayer);

playerInstances.forEach((playerInstance) => {
  playerInstance.setOnPlayHandler(onPlayHandler);
});
